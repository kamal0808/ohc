# ohc
Programming language that features programming in native Hindi language

Writing programs in native languages is  made possible with this project.
Online Hindi Compiler supports writing syntax in Hindi language, and doing basic
programming that involves taking user inputs, generating outputs, using conditional
if, if-else and looping statements.

Contributions to the project are welcome.

